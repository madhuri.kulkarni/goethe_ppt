# Goethe 360 Presentation using reveal.js

## Installation

`make install`

## Development environment

`make dev`

## Synopsis

Goethe 360 Presentation with reveal.js

## Installation

Requirements:

- Node.js

## Contributors

oneWave Studios GmbH & Co. KG

- Adrian Missy
- Nicolas Mercier
- Olivier Charvin
- Andre Leitol
- Madhuri Kulkarni

## Copyright

(c) 2019 oneWave Studios GmbH & Co. KG

## reveal.js

A framework for easily creating beautiful presentations using HTML.

## reveal.js License

MIT licensed

Copyright (C) 2019 Hakim El Hattab, http://hakim.se
